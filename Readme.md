# Azul Open JDK Suggestion API
This is a small project to provide downloadable Azul JDK 
URLs. I am providing a GIT link so the code can be downloaded
and run in any local machine.

GIT link: https://gitlab.com/muditgarg/azul_open_jdk_suggestion

"target" directory in GIT contains an executable JAR file which
can be run directly to test the application.
```bash
java -jar suggest_open_jdk-v1.jar
```

## Steps to run
1. Once code is checked out and opened in any IDE,
please run the following command:
```bash
mvn clean package
```
2. An executable JAR file will be generated with name:
   suggest_open_jdk-v1.jar
3. Run this JAR file in cmd using below command:
```bash
java -jar suggest_open_jdk-v1.jar
```
4. Code will return output in the following manner:
```bash
######################################################
Download details for Azul JDK 8
Package Name: zulu8.58.0.53-ca-fx-jdk8.0.312-macosx_x64.zip
Download URL: https://cdn.azul.com/zulu/bin/zulu8.58.0.53-ca-fx-jdk8.0.312-macosx_x64.zip
JDK Version: [8, 0, 312, 7]
Operating System: macos

######################################################

Download details are not available for Azul JDK 9

######################################################

Download details are not available for Azul JDK 10

######################################################

Download details for Azul JDK 11
Package Name: zulu11.52.51-ca-fx-jdk11.0.13-macosx_x64.zip
Download URL: https://cdn.azul.com/zulu/bin/zulu11.52.51-ca-fx-jdk11.0.13-macosx_x64.zip
JDK Version: [11, 0, 13, 8]
Operating System: macos

######################################################
```

## Requirements
1. The application should read system's OS and JDK 
details and based on them should suggest links.
2. Assumptions have been made that suggestions will be
made from JDK version 8 to 11.
3. If JDK version comes out to be less than 8, it 
should suggest Azul JDK URLs from version 8 to 11.
4. If a system is running JDK version 8, then also 
the application is suggesting JDK version 8 to 11.
5. In case, a system is running JDK version 10, then
the application will suggest JDK versions 10 and 11
only.
6. In output, following output can be seen:
```bash
######################################################

Download details are not available for Azul JDK 10

######################################################
```
It means the Azul API is not returning any downloadable
URL for JDK version 10 as input.

7. Sample Azul API used in code: 
https://api.azul.com/zulu/download/community/v1.0/bundles/latest/?java_version=11&jdk_version=11&os=macos&arch=x86&hw_bitness=64&ext=zip&bundle_type=jdk&support_term=lts


Author:
Mudit Garg
Email: mudit.garg188@gmail.com
Phone: 7028039818
