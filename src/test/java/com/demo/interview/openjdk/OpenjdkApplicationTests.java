package com.demo.interview.openjdk;

import com.demo.interview.openjdk.util.OpenJdkUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

/*
 * Unit Test class with following two test methods:
 * 1. suggest_azul_open_jdk_urls : test case to run whole application and if any exception occurs while running
 * (say no response from Azul API), then test case will be marked as failed, else passed.
 * 2. call_azul_api : test case to run Azul API with a sample URL
 */
@SpringBootTest
class OpenjdkApplicationTests {

	@Test
	void suggest_azul_open_jdk_urls() {
		OpenjdkApplication obj = new OpenjdkApplication();
		obj.callJdkSuggestionAPI();
	}

	@Test
	void call_azul_api() throws IOException {
		StringBuilder url = new StringBuilder
				("https://api.azul.com/zulu/download/community/v1.0/bundles/latest/?" +
						"java_version=11&jdk_version=11&os=macos&arch=x86&hw_bitness=64&ext=zip&" +
						"bundle_type=jdk&support_term=lts\n");
		String method = "GET";
		String response = OpenJdkUtil.callRestApi(url, method);
		System.out.print(response);
	}

}
