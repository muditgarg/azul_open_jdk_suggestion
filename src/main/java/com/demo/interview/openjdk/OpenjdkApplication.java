package com.demo.interview.openjdk;

import com.demo.interview.openjdk.service.IJdkDownloadableLinks;
import com.demo.interview.openjdk.service.impl.JdkDownloadableLinksImpl;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

/*
 * Main class of the application, which is called from cmd using command:
 * java -jar suggest_open_jdk-v1.jar
 *
 * Please refer to Readme.md for more details.
 */
@SpringBootApplication
public class OpenjdkApplication {

	static final Logger log = Logger.getLogger(OpenjdkApplication.class.getName());

	public static void main(String[] args) {
		log.info("Hello User! Azul JDK suggestion application is started!");
		OpenjdkApplication obj = new OpenjdkApplication();
		obj.callJdkSuggestionAPI();
	}

	public void callJdkSuggestionAPI() {
		IJdkDownloadableLinks obj = new JdkDownloadableLinksImpl();
		String output = obj.getAzulJdkVersions();
		log.info("Thank you so much for your patience, please find below downloadable Azul JDK links.");
		log.info(output);
	}

}
