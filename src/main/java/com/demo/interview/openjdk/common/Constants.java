package com.demo.interview.openjdk.common;

/*
 * Constant Interface to store application constant values
*/

public interface Constants {
    String ARCH = "x86";
    String HW_BITNESS = "64";
    String EXT = "zip";
    String BUNDLE_TYPE = "jdk";
    String SUPPORT_TERM = "lts";
    String BASE_URL = "https://api.azul.com/zulu/download/community/v1.0/bundles/latest/?";
    Integer MIN_JDK_VERSION = 8;
    Integer MAX_JDK_VERSION = 11;
}
