package com.demo.interview.openjdk.common;

/*
 * Enum containing Operating System names
 */
public enum OperatingSystem {
    windows, macos, linux, solaris
}
