package com.demo.interview.openjdk.service;

import com.demo.interview.openjdk.common.OperatingSystem;

/*
 * Interface to provide a blueprint for application logic.
 *
 * getJdkVersion : A static method to return system's JDK version.
 *
 * getOperatingSystemName : A static method to return system's Operating System name.
 *
 * Since getJdkVersion and getOperatingSystemName are meant for fixed functionalities, they are made static, so they
 * can be called using Interface name directly. There will be no requirement of these methods to be overridden.
 *
 * getAzulJdkVersions : An abstract method to be overridden by login implementing class.
 */
public interface IJdkDownloadableLinks {

    static String[] getJdkVersion() {
        String[] versionElements = System.getProperty("java.version").split("\\.");
        return versionElements;
    }

    static String getOperatingSystemName() {
        String osName = System.getProperty("os.name").toLowerCase();
        String detectedOs = "";
        if(osName.indexOf("mac") >= 0) {
            detectedOs = OperatingSystem.macos.toString();
        }
        else if (osName.indexOf("win") >= 0) {
            detectedOs = OperatingSystem.windows.toString();
        }
        else if (osName.indexOf("nux") >= 0 || osName.indexOf("nix") >= 0) {
            detectedOs = OperatingSystem.linux.toString();
        }
        else if (osName.indexOf("sunos") >= 0) {
            detectedOs = OperatingSystem.solaris.toString();
        }
        return detectedOs;
    }

    String getAzulJdkVersions();
}
