package com.demo.interview.openjdk.service.impl;

import com.demo.interview.openjdk.common.Constants;
import com.demo.interview.openjdk.service.IJdkDownloadableLinks;
import com.demo.interview.openjdk.util.OpenJdkUtil;
import com.demo.interview.openjdk.vo.Response;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/*
 * Business logic implementing class.
 *
 * Maintaining a list (toBeUpgradedJDKList) to maintain all JDK versions to be fetched from Azul API.
 * Min value of JDK version: 8 and max value of JDK version: 11
 *
 * Once system's JDK version is detected, same version and versions less than that are being removed from
 * toBeUpgradedJDKList list, so that only current and higher version JDKs can be suggested to a developer.
 *
 * Azul REST API is called with some static and some dynamic input parameters
 *
 * getAzulAPIResponse : Method to call Azul API with required input values
 *
 * getOutput: Method to generate a readable output
 */
@Service
public class JdkDownloadableLinksImpl implements IJdkDownloadableLinks {

    static final Logger log = Logger.getLogger(JdkDownloadableLinksImpl.class.getName());;
    private final List<Integer> toBeUpgradedJDKList = new ArrayList<>();

    {
        for(int i = Constants.MIN_JDK_VERSION; i <= Constants.MAX_JDK_VERSION; i++)
            toBeUpgradedJDKList.add(i);
    }

    @Override
    public String getAzulJdkVersions() {
        log.info("JDK Versions to be considered: "+toBeUpgradedJDKList);
        List<Response> responseList = new ArrayList<>();
        String detectedOs = IJdkDownloadableLinks.getOperatingSystemName();
        log.info("OS detected is: "+detectedOs);
        String[] javaVersionElements = IJdkDownloadableLinks.getJdkVersion();

        if(javaVersionElements.length > 0) {
            log.info("JDK version detected is: "+javaVersionElements[1]);
            int jdkVersion = Integer.parseInt(javaVersionElements[1]);
            if(jdkVersion >= Constants.MIN_JDK_VERSION) {
                List<Integer> temp = new ArrayList<>(toBeUpgradedJDKList);
                int index = 0;
                while(index < temp.size() && temp.get(index) < jdkVersion) {
                    toBeUpgradedJDKList.remove(temp.get(index));
                    index++;
                }
            }
        }
        log.info("Please hold back, preparing output for you...");
        responseList = getAzulAPIResponse(responseList, detectedOs);
        return getOutput(responseList);
    }

    private List<Response> getAzulAPIResponse(List<Response> responseList, String detectedOs) {
        toBeUpgradedJDKList.forEach(jdk -> {
            StringBuilder url = new StringBuilder(Constants.BASE_URL);
            url.append("java_version=").append(jdk);
            url.append("&jdk_version=").append(jdk);
            url.append("&os=").append(detectedOs);
            url.append("&arch="+ Constants.ARCH);
            url.append("&hw_bitness="+Constants.HW_BITNESS);
            url.append("&ext="+Constants.EXT);
            url.append("&bundle_type="+Constants.BUNDLE_TYPE);
            url.append("&support_term="+Constants.SUPPORT_TERM);
            String response = "";
            try {
                response = OpenJdkUtil.callRestApi(url, "GET");
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
            Gson gson = new Gson();
            Response responseBody = gson.fromJson(response, Response.class);
            responseList.add(responseBody);
        });
        return responseList;
    }

    private String getOutput(List<Response> responseList) {
        StringBuilder builder = new StringBuilder();
        AtomicInteger index = new AtomicInteger(0);
        builder.append("\n");
        builder.append("######################################################");
        responseList.forEach(response -> {
            if(response.getDetail() == null) {
                builder.append("\n");
                builder.append("Download details for Azul JDK ").append(toBeUpgradedJDKList.get(
                        Integer.parseInt(String.valueOf(index))));
                builder.append("\n");
                builder.append("Package Name: ").append(response.getName());
                builder.append("\n");
                builder.append("Download URL: ").append(response.getUrl());
                builder.append("\n");
                builder.append("JDK Version: ").append(response.getJdk_version());
                builder.append("\n");
                builder.append("Operating System: ").append(response.getOs());
                builder.append("\n");
            }
            else {
                builder.append("\n");
                builder.append("Download details are not available for Azul JDK ").append(toBeUpgradedJDKList.get(
                        Integer.parseInt(String.valueOf(index))));
                builder.append("\n");
            }
            builder.append("\n");
            builder.append("######################################################");
            builder.append("\n");
            index.addAndGet(1);
        });
        return builder.toString();
    }
}
