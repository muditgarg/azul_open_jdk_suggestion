package com.demo.interview.openjdk.util;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/*
 * A utility class to contain all util methods.
 *
 * callRestApi : Method to call any external REST API, input required : URL and Method type (GET, POST, PUT, etc.).
 * It returns JSON response from API in String format.
 */
public class OpenJdkUtil {

    public static String callRestApi(StringBuilder url, String methodType) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url.toString())
                .method(methodType, null)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
