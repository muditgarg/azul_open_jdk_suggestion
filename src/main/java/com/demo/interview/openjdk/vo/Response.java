package com.demo.interview.openjdk.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Response {
    private String name;
    private String url;
    private String os;
    private List<Integer> jdk_version;
    private String detail;
}
